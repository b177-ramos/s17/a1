// Activity Instructions:
// 1. In the S17 folder, create an a1 folder and an index.html and script.js file inside of it.
// 2. Link the script.js file to the index.html file.
// 3. Create an addStudent() function that will accept a name of the student and add it to the student array.
// 4. Create a countStudents() function that will print the total number of students in the array.
// 5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
// 6. Create a findStudent() function that will do the following:
// Search for a student name when a keyword is given (filter method).
// - If one match is found print the message studentName is an enrollee.
// - If multiple matches are found print the message studentNames are enrollees.
// - If no match is found print the message studentName is not an enrollee.
// - The keyword given should not be case sensitive.
// 7. Create a gitlab project repository named a1 in S17.
// 8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 9. Add the link in Boodle.


// Create an addStudent() function that will accept a name of the student and add it to the student array.
let students = [];
function addStudent (x){
	students.push(x);
	console.log(x + " was added to the student's list.");
}


// Create a countStudents() function that will print the total number of students in the array.
function countStudents(){
	console.log("There are a total of " + students.length + " students enrolled.");
}


// Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
function printStudents(){
	students.sort();
	students.forEach(student => console.log(student));
}


// Create a findStudent() function that will do the following:
// Search for a student name when a keyword is given (filter method).
// - If one match is found print the message studentName is an enrollee.
// - If multiple matches are found print the message studentNames are enrollees.
// - If no match is found print the message studentName is not an enrollee.
// - The keyword given should not be case sensitive.


/*let studentArray = [];
function addStudent (x){
	studentArray.push(x);
	console.log(x + " was added to the student's list.");*/

function findStudent(y){
	let filteredStudents = students.filter(function(students){
		return students.toLowerCase().includes(y.toLowerCase());
	})
	if (filteredStudents.length === undefined){
		console.log(y + " is not an enrollee");
	}
	else (filteredStudents.length !== undefined)
		console.log(y + " is an enrollee");
}

